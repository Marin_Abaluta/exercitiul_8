package ro.orangeprojects9;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List MyHotelEmployees = new ArrayList();

        MyHotelEmployees.add("Will Smith");
        MyHotelEmployees.add("Benjamin McCartney");
        MyHotelEmployees.add("Albert Davidson");
        MyHotelEmployees.add("Jaden Clinton");
        MyHotelEmployees.add("Donald Lincoln");
        MyHotelEmployees.add("Bart Bureau");
        MyHotelEmployees.add("Alistaire O'Doherty");


        System.out.println("The number of employees of the hotel is: " + MyHotelEmployees.size());

        /*System.out.println("Their names are: ["+ MyHotelEmployees.get(0) + ", " + MyHotelEmployees.get(1) + ", " +
        MyHotelEmployees.get(2) + ", " + MyHotelEmployees.get(3) + ", " + MyHotelEmployees.get(4) + ", " + MyHotelEmployees.get(5) +
        ", " + MyHotelEmployees.get(6) + "]."); */

        //System.out.println(Arrays.toString(MyHotelEmployees.toArray()));

        System.out.println("Their names are: " + MyHotelEmployees + ".");
        System.out.println("The employee at first floor is: " + MyHotelEmployees.get(1) + ".");
        MyHotelEmployees.remove(1);
        System.out.println("The designated employee on the first floor was fired for inappropiate behaviour.");
        System.out.println("The new list of employees is: " + MyHotelEmployees + ".");
        System.out.println("The new number of employees of the hotel is: " + MyHotelEmployees.size());

        for(int i=0;i<MyHotelEmployees.size();i++){
            System.out.println(MyHotelEmployees.get(i));
        }

        MyHotelEmployees.set(1,"Steve Irwin");

        System.out.println("The new substitute will be " + MyHotelEmployees.get(1) + ".");
        System.out.println("The final list of employees is :" + MyHotelEmployees + ".");

    }
}
